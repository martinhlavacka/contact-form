<!-- 
	
	CREDIT
	- - - - - - - - - - - - - - - - - - - - -
	Author: 	Martin Hlavacka
	Contact: 	martinhlavacka@outlook.com 
	Date: 		06.04.2018
			
	LICENSE
	- - - - - - - - - - - - - - - - - - - - -
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

	USAGE
	- - - - - - - - - - - - - - - - - - - - -
	1. Create folder called "backups"
	2. Update host, username, password and database values

-->

<!-- - - - - - - - - - 
SCRIPTS AND STYLESHEETS
- - - - - - - - - - -->

<!-- jQUERY -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<!-- BOOTSTRAP -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- FONT AWESOME -->
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>

<!-- - 
CONTENT
- - -->

<!-- STYLESHEET -->
<link rel="stylesheet" href="contact-form.css">

<!-- OPEN CONTACT FORM -->
<button type="button" class="btn btn-primary send-mail" data-toggle="modal" data-target="#contact-modal">
	<i class="fas fa-envelope leave-message-btn"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog contact-dialog" role="document">
		<div class="modal-content">
			
			<!-- HEADER -->
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Leave us a message</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="close-modal" aria-hidden="true">&times;</span>
				</button>
			</div>
			
			<!-- BODY -->
			<div class="modal-body">
				<div class="row">

					<!-- COMPANY DETAILS -->
					<div class="col-md-4 company-details-contact-form">
						
						<!-- COMAPNY NAME -->
						<h1>IMA WEB STUDIO</h1>
						
						<!-- COMPANY ADDRESS -->
						<p>Ditlev Bergs Vej 31</p>
						<p>9000 Aalborg</p>
						<p>Denmark</p>
						
						<!-- NEW LINE -->
						<br>

						<!-- CONTACT DETAILS -->
						<p><strong>Phone:</strong> +45 00 00 00 00</p>
						<p><strong>E-Mail:</strong> email@example.com</p>
						
						<!-- DIVIDER -->
						<hr>
						
						<!-- ADDRESS ON MAP -->
						<iframe class="google-map-contact" src="https://www.google.com/maps/embed/v1/place?q=Ditlev Bergs Vej 31, Aalborg&zoom=10&key=AIzaSyBHRjtCLxpd7S9yx-Z5qUIbInLoEKMWvY0"></iframe>
						
						<!-- DIVIDER -->
						<hr>

						<!-- SOCIAL NETWORKS -->
						<div class="social-icon">
							<i class="fab fa-facebook-f"></i>
						</div>

						<div class="social-icon">
							<i class="fab fa-twitter"></i>
						</div>

						<div class="social-icon">
							<i class="fab fa-google-plus-g"></i>
						</div>

						<div class="social-icon">
							<i class="fas fa-envelope"></i>
						</div>

						<div class="social-icon">
							<i class="fas fa-mobile-alt"></i>
						</div>
						
						<!-- DIVIDER -->
						<hr>

						<!-- SUBSCRIBE -->
						<span class="input-desc">SUBSCRIBE</span>
						<input type="email" placeholder="Insert your e-mail" class="subscribe-field  contact-form-input">
						
						<!-- SUBSCRIBE BUTTON -->
						<button class="button-subscribe-now">Subscribe now</button>
												
					</div>
					
					<!-- CONTACT FORM -->
					<div class="col-md-8 message-area-contact-form">

						<!-- FIELDS -->
						<div class="contact-form-fields">

							<!-- RESULT -->
							<div class="contact-form-result"></div>

							<!-- NAME -->
							<span class="input-desc">YOUR NAME</span>
							<input class="margin-bottom-15 contact-form-user contact-form-input" type="text" placeholder="Insert your name" required>

							<!-- E-MAIL -->
							<span class="input-desc">YOUR E-MAIL ADDRESS</span>
							<input class="margin-bottom-15 contact-form-email contact-form-input" type="email" placeholder="Insert your e-mail address" required>

							<!-- MESSAGE -->
							<span class="input-desc">YOUR MESSAGE</span>
							<textarea class="margin-bottom-15 contact-form-message contact-form-input" type="text" placeholder="Insert your message" cols="30" rows="17" required></textarea>

							<!-- IP ADDRESS (AJAX) -->
							<input type="text" class="contact-ip contact-form-input">

						</div>

						<!-- SPINNER -->
						<div class="spinner">
							<i class="fa fa-spinner fa-spin"></i>
						</div>

					</div>

				</div>
			</div>
			
			<!-- FOOTER -->
			<div class="modal-footer">
				<button type="button" class="contact-form-send-message"><i class="fab fa-telegram-plane"></i>Send message</button>
				<button type="button" class="contact-form-close-modal" data-dismiss="modal"><i class="far fa-times-circle"></i>Close</button>
			</div>

		</div>
	</div>
</div>